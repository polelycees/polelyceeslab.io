### carto des lycées d'île-de-france

Conversion des coordonnées Lambert93 (X,Y de IGN) en Géographique WGS84 (longitude/latitude)
`http://geofree.fr/gf/coordinateConv.asp#listSys`


Liste des établissements issus de l'opendata (coordonnées en Lambert93)
`http://www.data.gouv.fr/`

## Installation de Jekyll sur debian stretch

```
sudo gem install bundler --pre
sudo apt-get install ruby-dev
sudo apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev
bundle install
bundle exec jekyll serve
```

Accéder au site via http://120.0.0.1:4000
