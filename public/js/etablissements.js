var urlp  = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    attrs = "&copy; <a href=\"http://openstreetmap.org/copyright\">OpenStreetMap</a> contributors",
    osm   = L.tileLayer(urlp, { maxZoom: 18, attribution: attrs });

var i, etab, title, type, marker;

// Map
window.onload = function() {
    var map = L.map('map', { zoomControl: false });
    map.setView([ 48.8532023, 2.3407872 ], 9).addLayer(osm);

    map.addControl(L.control.zoom({ position: 'bottomleft' }));

	var greenIcon = L.icon({
			iconUrl:"images/marker-green-3.png",
			iconSize: L.point(30, 30)
	})

	var redIcon = L.icon({
			iconUrl:"images/marker-red-3.png",
			iconSize: L.point(30, 30)
	})

  var yellowIcon = L.icon({
			iconUrl:"images/marker-yellow-3.png",
			iconSize: L.point(30, 30)
	})
  var blueIcon = L.icon({
      iconUrl:"images/marker-blue-3.png",
      iconSize: L.point(30, 30)
  })
  var wifi0Icon = L.icon({
      iconUrl:"images/step0.png",
      iconSize: L.point(30, 30)
  })
  var wifi1Icon = L.icon({
      iconUrl:"images/step1.png",
      iconSize: L.point(30, 30)
  })
  var wifi2Icon = L.icon({
      iconUrl:"images/step2.png",
      iconSize: L.point(30, 30)
  })
  var wifi3Icon = L.icon({
      iconUrl:"images/step3.png",
      iconSize: L.point(30, 30)
  })
  var wifi4Icon = L.icon({
      iconUrl:"images/step4.png",
      iconSize: L.point(30, 30)
  })
  var markers = []
	for (i in etabMarkers) {
	    etab  = etabMarkers[i]
        markers[i] = L.marker([ parseFloat(etab.latitude.replace(/,/, '.')),
  	                        parseFloat(etab.longitude.replace(/,/, '.')) ],
  	                      { data: etab, icon: redIcon });

        var content = '<div class="etab-type-lyc">' +
                      '<h2><a href="' + etab.SITE_WEB + '">' + etab.PATRONYME + '</a></h2>' +
                      '<h4>' + etab.COMMUNE + '</h4>' +
  	                  '<p class="uai">' + etab.UAI + '</p>'  +
                      '<p class="tel"><a " href="tel:0'+etab.TELEPHONE+'">0' + etab.TELEPHONE + '</a></p>' +
  	                  '</div>'
        var options = {
          className : 'myPopup'
        }
  	    markers[i].bindPopup(content, options);
        markers[i].addTo(map);
	}
  var numeric_colleges = function() {
    document.getElementById('mainTitle').innerHTML = "Les lycées &#171; 100% numérique &#187; en Île-de-France"
    for (i in etabMarkers) {
  	    etab  = etabMarkers[i]
        if (etab.NUMERIQUE == 'OUI') {
          markers[i]._icon.style.visibility = 'visible'
          markers[i].setIcon(greenIcon)
        }
        else {

          markers[i]._icon.style.visibility = 'hidden'
        }
  	}
  }
  var standalone_colleges = function() {
    document.getElementById('mainTitle').innerHTML = "Les lycées &#171; autonomie &#187; en Île-de-France"
    for (i in etabMarkers) {
  	    etab  = etabMarkers[i]
        if (etab.AUTONOMIE != '') {

          markers[i]._icon.style.visibility = 'visible'
          markers[i].setIcon(blueIcon)
        }
        else {
          markers[i]._icon.style.visibility = 'hidden'
        }
  	}
  }
  var all_colleges = function() {
    document.getElementById('mainTitle').innerHTML = "Les lycées en Île-de-France"
    for (i in etabMarkers) {
      markers[i]._icon.style.visibility = 'visible'
      markers[i].setIcon(redIcon)
  	}
  }
  var wifi = function() {
    document.getElementById('mainTitle').innerHTML = "Déploiement du wifi"
    for (i in etabMarkers) {

      etab  = etabMarkers[i]
      if (etab.hasOwnProperty('wifi')) {
        markers[i]._icon.style.visibility = 'visible'
        console.log(etab.wifi)
        if (etab.wifi.step == "0") markers[i].setIcon(wifi0Icon)
        if (etab.wifi.step == "1") markers[i].setIcon(wifi1Icon)
        if (etab.wifi.step == "2") markers[i].setIcon(wifi2Icon)
        if (etab.wifi.step == "3") markers[i].setIcon(wifi3Icon)
        if (etab.wifi.step == "4") markers[i].setIcon(wifi4Icon)
      }
      else {
        markers[i]._icon.style.visibility = 'hidden'
      }

  	}
  }
  document.getElementById('all_colleges').addEventListener('click', all_colleges, false)
  document.getElementById('numeric_colleges').addEventListener('click', numeric_colleges, false)
  document.getElementById('standalone_colleges').addEventListener('click', standalone_colleges, false)
  document.getElementById('wifi').addEventListener('click', wifi, false)


}
